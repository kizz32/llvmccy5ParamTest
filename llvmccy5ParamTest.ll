target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"
; Begin asmlist al_begin
; Syms - Begin Staticsymtable
	%"typ.LLVMCCY5PARAMTEST.TTestObjFail" = type <{ ptr }>
; Syms - End Staticsymtable
	%"typ.LLVMCCY5PARAMTEST.TTestObjFail.$vmtdef" = type <{ i64, i64, ptr, ptr }>
	%"typ.LLVMCCY5PARAMTEST.00000005" = type <{ i64, i64, ptr, ptr }>
	%"typ.LLVMCCY5PARAMTEST.00000006" = type <{ i32, i8, i8, i8, i8, ptr }>
	%"typ.LLVMCCY5PARAMTEST.00000007" = type <{ i64 }>
	%"typ.LLVMCCY5PARAMTEST.00000008" = type <{ i64 }>
	%"typ.LLVMCCY5PARAMTEST.00000009" = type <{ i64 }>
	%"typ.SYSTEM.FPC_Unwind_Exception" = type <{ i64, ptr, i64, i64, i64, i64, i64, i64 }>
	%"typ.SYSTEM.FPC_Unwind_Context" = type <{  }>
; End asmlist al_begin
; Begin asmlist al_procedures
define ptr @"\01P$LLVMCCY5PARAMTEST$_$TTESTOBJFAIL_$__$$_INIT$LONGINT$LONGINT$LONGINT$LONGINT$CURRENCY$$QWORDBOOL"(ptr nocapture dereferenceable_or_null(8) %p.$self, ptr %p.$vmt, i32 %p.P1, i32 %p.P2, i32 %p.P3, i32 %p.P4, i64 %p.P5) nobuiltin null_pointer_is_valid strictfp personality ptr @"\01SYSTEM_$$__FPC_PSABIEH_PERSONALITY_V0$hs2HxE9SEE3H" {
	%tmp.1 = alloca i32, align 8
	%tmp.2 = alloca i32, align 8
	%tmp.3 = alloca i32, align 8
	%tmp.4 = alloca i32, align 8
	%tmp.5 = alloca i64, align 8
	%tmp.6 = alloca ptr, align 8
	%tmp.7 = alloca ptr, align 8
	%reg.1_17 = bitcast ptr %tmp.1 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 4, ptr %reg.1_17)
	%reg.1_19 = bitcast ptr %tmp.2 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 4, ptr %reg.1_19)
	%reg.1_21 = bitcast ptr %tmp.3 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 4, ptr %reg.1_21)
	%reg.1_23 = bitcast ptr %tmp.4 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 4, ptr %reg.1_23)
	%reg.1_25 = bitcast ptr %tmp.5 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 8, ptr %reg.1_25)
	%reg.1_27 = bitcast ptr %tmp.6 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 8, ptr %reg.1_27)
	%reg.1_29 = bitcast ptr %tmp.7 to ptr
	call  void (i64, ptr) @llvm.lifetime.start (i64 8, ptr %reg.1_29)
	store ptr %p.$self, ptr %tmp.7, align 8
	store ptr %p.$vmt, ptr %tmp.6, align 8
	%reg.1_30 = bitcast i32 %p.P1 to i32
	store i32 %reg.1_30, ptr %tmp.1, align 8
	%reg.1_31 = bitcast i32 %p.P2 to i32
	store i32 %reg.1_31, ptr %tmp.2, align 8
	%reg.1_32 = bitcast i32 %p.P3 to i32
	store i32 %reg.1_32, ptr %tmp.3, align 8
	%reg.1_33 = bitcast i32 %p.P4 to i32
	store i32 %reg.1_33, ptr %tmp.4, align 8
	%reg.2_9 = fptrunc x86_fp80 %p.P5 to i64
	%reg.2_10 = fptosi x86_fp80 %reg.2_9 to i64
	store i64 %reg.2_10, ptr %tmp.5, align 8
	%reg.1_34 = bitcast ptr %tmp.6 to ptr
	%reg.1_35 = load ptr, ptr %tmp.7, align 8
	%reg.1_37 = call  ptr (ptr, ptr, i32) @"\01fpc_help_constructor" (ptr %reg.1_35, ptr %reg.1_34, i32 0)
	%reg.1_38 = bitcast ptr %reg.1_37 to ptr
	store ptr %reg.1_38, ptr %tmp.7, align 8
	%reg.1_39 = load ptr, ptr %tmp.7, align 8
	%reg.1_40 = inttoptr i64 0 to ptr
	%reg.1_41 = icmp eq ptr %reg.1_39, %reg.1_40
	%reg.1_42 = zext i1 %reg.1_41 to i8
	%reg.1_43 = trunc i64 0 to i8
	%reg.1_44 = icmp ne i8 %reg.1_42, %reg.1_43
	br i1 %reg.1_44, label %.Lj5, label %.Lj7
.Lj7:
	br label %.Lj6
.Lj5:
	br label %.Lj3
	br label %.Lj6
.Lj6:
	br label %.Lj3
.Lj3:
	%reg.1_46 = bitcast ptr %tmp.1 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 4, ptr %reg.1_46)
	%reg.1_48 = bitcast ptr %tmp.2 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 4, ptr %reg.1_48)
	%reg.1_50 = bitcast ptr %tmp.3 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 4, ptr %reg.1_50)
	%reg.1_52 = bitcast ptr %tmp.4 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 4, ptr %reg.1_52)
	%reg.1_54 = bitcast ptr %tmp.5 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 8, ptr %reg.1_54)
	%reg.1_56 = bitcast ptr %tmp.6 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 8, ptr %reg.1_56)
	%reg.1_0 = load ptr, ptr %tmp.7, align 8
	%reg.1_58 = bitcast ptr %tmp.7 to ptr
	call  void (i64, ptr) @llvm.lifetime.end (i64 8, ptr %reg.1_58)
	ret ptr %reg.1_0
}
@"\01PASCALMAIN" = alias  void (), ptr @"\01main"
define void @"\01main"() nobuiltin noreturn null_pointer_is_valid strictfp {
	call  void () @"\01fpc_initializeunits" ()
	%reg.2_9 = sitofp i64 50000 to x86_fp80
	%reg.2_10 = bitcast x86_fp80 %reg.2_9 to x86_fp80
	%reg.1_16 = bitcast ptr @"\01VMT_$P$LLVMCCY5PARAMTEST_$$_TTESTOBJFAIL" to ptr
	%reg.1_17 = bitcast ptr %reg.1_16 to ptr
	%reg.1_18 = bitcast ptr @"\01U_$P$LLVMCCY5PARAMTEST_$$_TESTOBJ" to ptr
	%reg.1_23 = call  ptr (ptr, ptr, i32, i32, i32, i32, i64) @"\01P$LLVMCCY5PARAMTEST$_$TTESTOBJFAIL_$__$$_INIT$LONGINT$LONGINT$LONGINT$LONGINT$CURRENCY$$QWORDBOOL" (ptr %reg.1_18, ptr %reg.1_17, i32 1, i32 2, i32 3, i32 4, i64 %reg.2_10)
	br label %.Lj1
.Lj1:
	call  void () @"\01fpc_do_exit" ()
	ret void
}
declare i32 @"\01SYSTEM_$$__FPC_PSABIEH_PERSONALITY_V0$hs2HxE9SEE3H"(i32, i32, i64, ptr, ptr) nobuiltin null_pointer_is_valid strictfp
declare void @llvm.lifetime.start(i64, ptr) null_pointer_is_valid strictfp
declare ptr @"\01fpc_help_constructor"(ptr, ptr nocapture dereferenceable_or_null(8), i32) nobuiltin null_pointer_is_valid strictfp
declare void @llvm.lifetime.end(i64, ptr) null_pointer_is_valid strictfp
declare void @"\01fpc_initializeunits"() nobuiltin null_pointer_is_valid strictfp
declare void @"\01fpc_do_exit"() nobuiltin null_pointer_is_valid strictfp
; End asmlist al_procedures
; Begin asmlist al_globals
@"\01U_$P$LLVMCCY5PARAMTEST_$$_TESTOBJ" = hidden global %"typ.LLVMCCY5PARAMTEST.TTestObjFail" zeroinitializer, align 8
@"\01VMT_$P$LLVMCCY5PARAMTEST_$$_TTESTOBJFAIL" = constant %"typ.LLVMCCY5PARAMTEST.TTestObjFail.$vmtdef" <{i64 8, i64 -8, ptr zeroinitializer, ptr zeroinitializer }>, align 8
@"\01INITFINAL" = global %"typ.LLVMCCY5PARAMTEST.00000005" <{i64 1, i64 zeroinitializer, ptr @"\01INIT$_$SYSTEM", ptr zeroinitializer }>, align 8
@"\01FPC_THREADVARTABLES" = global %"typ.LLVMCCY5PARAMTEST.00000006" <{i32 1, i8 zeroinitializer, i8 zeroinitializer, i8 zeroinitializer, i8 zeroinitializer, ptr @"\01THREADVARLIST_$SYSTEM$indirect" }>, align 8
@"\01FPC_RESOURCESTRINGTABLES" = constant %"typ.LLVMCCY5PARAMTEST.00000007" <{i64 zeroinitializer }>, align 8
@"\01FPC_WIDEINITTABLES" = global %"typ.LLVMCCY5PARAMTEST.00000008" <{i64 zeroinitializer }>, align 8
@"\01FPC_RESSTRINITTABLES" = global %"typ.LLVMCCY5PARAMTEST.00000009" <{i64 zeroinitializer }>, align 8
@"\01__fpc_ident" = internal global [41 x i8] c"FPC 3.3.1 [2023/11/02] for x86_64 - Linux", align 32
@"\01__stklen" = global i64 8388608, align 8
@"\01__heapsize" = global i64 zeroinitializer, align 8
@"\01__fpc_valgrind" = global i8 zeroinitializer, align 8
@"\01FPC_RESLOCATION" = constant ptr zeroinitializer, align 8
@llvm.compiler.used = appending global [1 x ptr] [ptr @"\01__fpc_ident"], section "llvm.metadata"
@"\01INIT$_$SYSTEM" = external global ptr, align 8
@"\01THREADVARLIST_$SYSTEM$indirect" = external global ptr, align 8
; End asmlist al_globals
; Begin asmlist al_rotypedconsts
!llvm.module.flags = !{}
; End asmlist al_rotypedconsts
; Begin asmlist al_indirectglobals
@"\01VMT_$P$LLVMCCY5PARAMTEST_$$_TTESTOBJFAIL$indirect" = constant ptr @"\01VMT_$P$LLVMCCY5PARAMTEST_$$_TTESTOBJFAIL", align 8
; End asmlist al_indirectglobals

