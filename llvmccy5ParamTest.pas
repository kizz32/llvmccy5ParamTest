program llvmccy5ParamTest;

type

	TTestObjFail = object
		constructor Init(
			P1, P2, P3, P4: LongInt; P5: Currency);
	end;

constructor TTestObjFail.Init(
			P1, P2, P3, P4: LongInt; P5: Currency);
begin
end;

var
	TestObj	: TTestObjFail;
begin
	TestObj.Init(1, 2, 3, 4, 5);
end.
